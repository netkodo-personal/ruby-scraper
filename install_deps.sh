#!/bin/bash

# installs watir: beta
gem install watir --pre

# installs selenium: beta
gem install selenium-webdriver --pre

# download and install geckodrive
# # source: https://stackinstall.com/how-to-install-geckodriver-on-ubuntu-16-04/
cd "$HOME"
# # downloads geckodrive version 0.29.1 x64-bit for linux
wget -O "geckodriver-v0.29.1-linux64.tar.gz" "https://github.com/mozilla/geckodriver/releases/download/v0.29.1/geckodriver-v0.29.1-linux64.tar.gz"
# # unpacks it with tar
tar xf "geckodriver-v0.29.1-linux64.tar.gz"
# # copies unpacked geckodrive to /usr/local/bin/
sudo cp "geckodriver" "/usr/local/bin/geckodriver"
# # gives executable privilege to copied geckodrive
sudo chmod +x "/usr/local/bin/geckodriver"
# # deletes downloaded remains
rm -f "geckodriver"
rm -f "geckodriver-v0.29.1-linux64.tar.gz"