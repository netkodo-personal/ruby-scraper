# frozen_string_literal: true

require 'watir'
require 'tempfile'
require 'rmagick'
# weird but rmagick says that it should be that way
# require Magick

module PartScreenshot
  # @param [Watir::Browser] browser
  # @param [Watir::HTMLElement] element
  # @param [String] image_name
  # @return [NilClass]
  def crop_element(browser, element, image_name)
    # broken down for readability
    # calculating points of origin and width and height of element
    x = element.wd.location.x.to_i + 1
    y = element.wd.location.y.to_i + 1
    w = element.wd.size.width.to_i
    h = element.wd.size.height.to_i

    browser = auto_scroll(browser, y, h)

    # # recalculating after change
    y = element.wd.location.y.to_i
    h = element.wd.size.height.to_i


    # creating temporary file from Ruby Tempfile.
    # it makes uniq name so it won't create potential conflict
    file = Tempfile.new(image_name)
    begin
      # browser saves screenshot of viewable page that can be seen.
      browser.screenshot.save(file)

      # opening in RMagick temporary image, cropping it to calculated size and writing to . place with given name as jpg
      image = Magick::ImageList.new(file.path)
      image.display
      image = image.crop(x, y, w, h)
      image.display
      image.write("#{image_name}.jpg")
    ensure
      # closing temporary file, will happen whether we succeed or fail
      file.close(true)
    end
  end

  # @param [Watir::browser] browser
  # @param [int] h
  # @param [int] y
  def auto_scroll(browser, y, h)
    amount = 0
    amount = y + h - browser.window.size[1] + 10 if y + h > browser.window.size[1]
    browser.execute_script("window.scrollBy(0, #{amount*1.5})")
    browser
  end
end
