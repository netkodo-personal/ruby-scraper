require 'watir'

# options for user to set
car = 'abarth'
pages_to_scrap = 10


browser = Watir::Browser.new :firefox, headless: true

# going to otomoto.pl
browser.goto 'https://www.otomoto.pl/'

# cookie
browser.button(id: 'onetrust-accept-btn-handler').click
sleep(1)

# searching car
browser.text_field(class: 'ds-select').set car

sleep(1)
# enter to make above field select Toyota
browser.send_keys :enter
# wait to make sure that server knows what we choose
sleep(1)
# and enter to search for those cars
browser.send_keys :enter

# wait ot load page
sleep(3)

# prepared vars
index = 0
hash_db = []


while pages_to_scrap > 0
  p pages_to_scrap
  cars = browser.articles(class: 'offer-item', data_test: 'search-result-item')
  cars.each do |x|
    temp_hash = { id: index, title: x.a(class: 'offer-title__link').title }

    # selecting ul of interest
    ul = x.ul(class: 'ds-params-block')
    # taking year of production, mileage, engine capacity, fuel type
    # if ul.count != 4
    #   # otomoto offers 'carsmile' offerings have 2 the same ul's with same classes.
    #   # because they are one after another i can just select next by the index or by .following_sibling
    #
    #   # price
    #   pr = x.span(class: 'offer-price__number', data_test: 'ad-card-price-carsmile')
    #   temp_hash[:price] = pr.text.to_s
    #   # broken on 2 lines for more readability
    #
    #   ul = x.ul(class: 'ds-params-block').following_sibling
    #   temp_hash[:year] = ul.li(data_code: '0').span.text
    #   temp_hash[:mileage] = ul.li(data_code: '1').span.text
    #   temp_hash[:engine_capacity] = ul.li(data_code: '2').span.text
    #   temp_hash[:fuel_type] = ul.li(data_code: '3').span.text
    # else
    if ul.count == 4
      # price currently on hiatus
      temp_hash[:price] = x.span(class: 'offer-price__number').text

      temp_hash[:year] = ul.li(data_code: 'year').span.text
      temp_hash[:mileage] = ul.li(data_code: 'mileage').span.text
      temp_hash[:engine_capacity] = ul.li(data_code: 'engine_capacity').span.text
      temp_hash[:fuel_type] = ul.li(data_code: 'fuel_type').span.text
    end

    hash_db << temp_hash
    index += 1
  end

  p '=================================='
  # next page
  if browser.li(class: 'next').exists?
    browser.li(class: 'next').click
    pages_to_scrap -= 1
  else
    p 'Left prematurly, not enough pages to scrap.'
    break
  end
  sleep(2)
end

p hash_db

# cleaning
browser.close
